var mongoose = require('mongoose');
var BaseModel = require("./base_model");
var Schema = mongoose.Schema;
var _ = require('lodash');
var ObjectId = mongoose.Schema.Types.ObjectId;
var Tag = require('./tag');
var User = require('./user');
var RoundTable = require('./roundtable');

var RoundTableSchema = new Schema({
    user_id : {type : ObjectId, ref : User},
    name : {type : String},
    is_private : {type : Boolean, default : false},
    is_storaged : {type : Boolean, default : true},
    size : {type : Number},
    members : [{
        user_id : {type : ObjectId, ref : User},
        name : {type : String},
        avatar : {type : String}
    }],
    descriptions : {type : String},
    tags : [{
        id : {type : ObjectId, ref : Tag},
        name : {type : String},
    }],    
    start_date : {type : Date},
    end_date : {type : Date},
    create_at: { type: Date, default: Date.now },
    update_at: { type: Date, default: Date.now },
});

RoundTableSchema.plugin(BaseModel);

RoundTableSchema.pre('save', function (next) {
    var now = new Date();
    this.update_at = now;
    next();
});

mongoose.model('RoundTable', RoundTableSchema);