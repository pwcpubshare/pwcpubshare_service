var mongoose = require('mongoose');
var BaseModel = require("./base_model");
var Schema = mongoose.Schema;
var _ = require('lodash');
var ObjectId = mongoose.Schema.Types.ObjectId;
var User = require('./user');

var UserFollowerSchema = new Schema({
    user_id : {type : ObjectId , ref : User},
    followers : [{
        id : {type : ObjectId, ref : User}
    }],
    create_at: { type: Date, default: Date.now },
    update_at: { type: Date, default: Date.now },
});

UserFollowerSchema.plugin(BaseModel);

UserFollowerSchema.pre('save', function (next) {
    var now = new Date();
    this.update_at = now;
    next();
});

mongoose.model('UserFollower', UserFollowerSchema);