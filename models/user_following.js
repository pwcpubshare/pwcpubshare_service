var mongoose = require('mongoose');
var BaseModel = require("./base_model");
var Schema = mongoose.Schema;
// var utility = require('utility');
var _ = require('lodash');
var ObjectId = mongoose.Schema.Types.ObjectId;
var User = require('./user');

var UserFollowingSchema = new Schema({
    user_id : {type : ObjectId , ref : User},
    followings : [{
        id : {type : ObjectId, ref : User}
    }],
    create_at: { type: Date, default: Date.now },
    update_at: { type: Date, default: Date.now },
});

UserFollowingSchema.plugin(BaseModel);

UserFollowingSchema.pre('save', function (next) {
    var now = new Date();
    this.update_at = now;
    next();
});

mongoose.model('UserFollowing', UserFollowingSchema);