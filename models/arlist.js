var mongoose = require('mongoose');
var BaseModel = require("./base_model");
var Schema = mongoose.Schema;
var _ = require('lodash');
var ObjectId = mongoose.Schema.Types.ObjectId;

var ARListSchema = new Schema({
    user_id : {type : ObjectId, index : true},
    name : {type : String},
    image : {type : String},
    video : {type : String},
    create_at: { type: Date, default: Date.now },
    update_at: { type: Date, default: Date.now },
});

ARListSchema.plugin(BaseModel);

ARListSchema.pre('save', function (next) {
    var now = new Date();
    this.update_at = now;
    next();
});

mongoose.model('ARList', ARListSchema);