var mongoose = require('mongoose');
var BaseModel = require("./base_model");
var Schema = mongoose.Schema;
var _ = require('lodash');
var ObjectId = mongoose.Schema.Types.ObjectId;

var FocusAreaSchema = new Schema({
    name : {type : String},
    image : {type : String},
    create_at: { type: Date, default: Date.now },
    update_at: { type: Date, default: Date.now },
});

FocusAreaSchema.plugin(BaseModel);

FocusAreaSchema.pre('save', function (next) {
    var now = new Date();
    this.update_at = now;
    next();
});

mongoose.model('FocusArea', FocusAreaSchema);