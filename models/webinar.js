var mongoose = require('mongoose');
var BaseModel = require("./base_model");
var Schema = mongoose.Schema;
var _ = require('lodash');
var ObjectId = mongoose.Schema.Types.ObjectId;
var User = require('./user');
var Tag = require('./tag');

var WebinarSchema = new Schema({
    user_id : {type : ObjectId, ref : User},
    title : {type : String},
    date : {type : String},
    path : {type : String},
    likers:[{
        user_id : {type : ObjectId, ref : User}
    }],
    comments : [{
        user_id : {type : ObjectId, ref : User},
        body : {type : String},
        type : {type : Number}//0 : text, 1 : image, 2:video
    }],  
    tags : [{
        id : {type : ObjectId, ref : Tag},
        name : {type : String},
    }],
    create_at: { type: Date, default: Date.now },
    update_at: { type: Date, default: Date.now },
});

WebinarSchema.plugin(BaseModel);

WebinarSchema.pre('save', function (next) {
    var now = new Date();
    this.update_at = now;
    next();
});

mongoose.model('Webinar', WebinarSchema);