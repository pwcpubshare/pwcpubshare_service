var mongoose = require('mongoose');
var config = require('../config');
var logger = require('../common/logger')

mongoose.connect(config.db, {
    server: { poolSize: 20 }
}, function (err) {
    if (err) {
        logger.error('connect to %s error: ', config.db, err.message);
        process.exit(1);
    }
});

// models
require('./user');
require('./arlist');
require('./focus_area');
require('./roundtable');
require('./topic');
require('./user_follower');
require('./user_following');
require('./webinar');
require('./tag');

exports.User = mongoose.model('User');
exports.ARList = mongoose.model('ARList');
exports.FocusArea = mongoose.model('FocusArea');
exports.RoundTable = mongoose.model('RoundTable');
exports.UserFollower = mongoose.model('UserFollower');
exports.UserFollowing = mongoose.model('UserFollowing');
exports.Webinar = mongoose.model('Webinar');
exports.Topic = mongoose.model('Topic');
exports.Tag = mongoose.model('Tag');