var mongoose = require('mongoose');
var BaseModel = require("./base_model");
var Schema = mongoose.Schema;
var _ = require('lodash');
var ObjectId = mongoose.Schema.Types.ObjectId;
var User = require('./user');
var RoundTable = require('./roundtable');


var TopicSchema = new Schema({
    user_id : {type : ObjectId, ref : User},
    title : {type : String},
    description : {type : String},
    resources : [{
        type : {type : Number}, //1 : image, 2 : video
        path : {type : String}
    }],
    likers:[{
        user_id : {type : ObjectId, ref : User}
    }],
    comments : [{
        user_id : {type : ObjectId, ref : User},
        body : {type : String},
        type : {type : Number}//1 : text, 2 : image, 3:video
    }],
    roundtable_id : {type : ObjectId, ref : RoundTable},
    create_at: { type: Date, default: Date.now },
    update_at: { type: Date, default: Date.now },
});

TopicSchema.plugin(BaseModel);

TopicSchema.pre('save', function (next) {
    var now = new Date();
    this.update_at = now;
    next();
});

mongoose.model('Topic', TopicSchema);