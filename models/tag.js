var mongoose = require('mongoose');
var BaseModel = require("./base_model");
var Schema = mongoose.Schema;
// var utility = require('utility');
var _ = require('lodash');
var ObjectId = mongoose.Schema.Types.ObjectId;

var TagSchema = new Schema({
    name : {type : String},
    image : {type : String},
    create_at: { type: Date, default: Date.now },
    update_at: { type: Date, default: Date.now },
});

TagSchema.plugin(BaseModel);

TagSchema.pre('save', function (next) {
    var now = new Date();
    this.update_at = now;
    next();
});

mongoose.model('Tag', TagSchema);