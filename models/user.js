var mongoose = require('mongoose');
var BaseModel = require("./base_model");
var Schema = mongoose.Schema;
// var utility = require('utility');
var ObjectId = mongoose.Schema.Types.ObjectId;
var Tag = require('./tag');
var _ = require('lodash');

var UserSchema = new Schema({
    first_name: { type: String },
    last_name: { type: String },
    avatar: { type: String },
    email : {type : String, index:true},
    password : {type : String},
    title : {type : String},
    signature : {type : String},
    followed_tags : [{
        id : {type :ObjectId , ref : Tag},
        name:{type : String}
    }],
    create_at: { type: Date, default: Date.now },
    update_at: { type: Date, default: Date.now },
    access_token: {type : String}
});

UserSchema.plugin(BaseModel);

UserSchema.virtual('fullname').get(function () {    
    return this.first_name + this.last_name;
});

UserSchema.pre('save', function (next) {
    var now = new Date();
    this.update_at = now;
    next();
});

mongoose.model('User', UserSchema);