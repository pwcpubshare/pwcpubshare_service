var models = require('../models');
var User = models.User;

exports.init = function (info, cb) {
  var user = new User();
  user.first_name = info.first_name;
  user.last_name = info.last_name;
  user.avatar = info.avatar;
  user.email = info.email;
  user.password = info.password;
  user.signature = info.signature;
  user.title = info.title;  
  user.save(cb);
};


exports.verifyEmailAndPass = function(email, pass, cb){
    User.findOne({email : email, password : pass}).exec(cb);
};

exports.favoriteTags = function(userId, tags, cb){
  User.update({'_id' : userId},{
    followed_tags : tags
  },{}, cb);
};