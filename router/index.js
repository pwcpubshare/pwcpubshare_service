var express = require('express');
var router = express.Router();

var controllerFactory = require('../controller');

//登录
router.post('/login', controllerFactory.User.signIn);


router.all("*", function (req, res, next) {
    var basic = req.headers['authorization'] || '';
    console.log(basic);
    var basicString = new Buffer(basic.substring(6,basic.length), 'base64').toString().split(':');
    console.log(basicString);
    if (req.method === "OPTIONS") {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Authorization, Content-Type");
        res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
        return res.end();
    } else {
        var userId = basicString[0];
        var email = basicString[1];

        res.setHeader("Content-Type", "application/json;charset='utf-8'");
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Authorization");
        res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
        var user = {
            id: userId,
            email: email
        };
        req.session.user = user;
        return next();
    }
});

//个人信息
router.get('/user/:id', controllerFactory.User.info);
//首页
router.get('/homepage', controllerFactory.Application.home);
//发现roundtable
router.get('/roundtable/list', controllerFactory.RoundTable.list);
//发布roundtable
router.post('/roundtable', controllerFactory.RoundTable.create);
router.get('/roundtable/:id', controllerFactory.RoundTable.detail);
router.get('/roundtable/:id/comment/list', controllerFactory.RoundTable.comments);
router.post('/roundtable/:id/comment', controllerFactory.RoundTable.postComment);

//获取所有tag列表
router.get('/tag/list', controllerFactory.Application.tags);
router.post('/tag/favorite', controllerFactory.User.favoriteTag);

module.exports = router;